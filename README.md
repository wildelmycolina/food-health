# Food Health

### Despliegue

Este proyecto está desplegado en vercel [https://food-health-ten.vercel.app](https://food-health-ten.vercel.app)


## Documentación

Food Health está hecho con React + TypeScript + Vite + TailwindCSS

### Estructura

En la carpeta **./src** estan los archivos principales. Haz clic sobre los nombres para ver más información
<details><summary>assets</summary>

- icon: almacena los iconos de la aplicación
- img: almacena las imágenes de la aplicación
- logo: almacena el logo de la aplicación

</details>

<details><summary>components</summary>

- cart: almacena los componentes del carrito de compras
- categories: almacena los componentes del carousel de categorias
- food-status: almacena los componentes del listado de estado de las comidas (order, delivered, finished)
- AppHero.tsx: Compente hero principal de la aplicación
- AppNavegation.tsx: Componente de la navegación lateral
- AppStatistics.tsx: Compente de las cuatro estadísticas de la aplicación
</details>

<details><summary>helpers</summary>

- animationFrame.ts: Este helper se usa para realizar animaciones usando la api **requestAnimationFrame**, lo que permite realizar actualizaciones de namera fluida sin afectar el rendimiento en el navegador. En este caso, la usamos para realizar la transición de aumento de los números en las estadísticas de la aplicación
 > argumentos:
 >> callback: La función a ejecutar de manera progresiva. Está función recibe como parametro un número que va subiendo desde **0** a **1**, que indica el progreso de la transición

 >> duration: Duración de la transición en milisegundos

 >> delay: Tiempo de espera para que inicie la transición
</details>

<details><summary>layouts</summary>

- MainLayout.tsx: Layout principal donde contenemos nuestra aplicación
</details>

## Proceso de instalación


```js
 git clone https://gitlab.com/wildelmycolina/food-health.git 

 npm install

 npm run dev

 enjoy :)!
```


## React + TypeScript + Vite

This template provides a minimal setup to get React working in Vite with HMR and some ESLint rules.

Currently, two official plugins are available:

- [@vitejs/plugin-react](https://github.com/vitejs/vite-plugin-react/blob/main/packages/plugin-react/README.md) uses [Babel](https://babeljs.io/) for Fast Refresh
- [@vitejs/plugin-react-swc](https://github.com/vitejs/vite-plugin-react-swc) uses [SWC](https://swc.rs/) for Fast Refresh

## Expanding the ESLint configuration

If you are developing a production application, we recommend updating the configuration to enable type aware lint rules:

- Configure the top-level `parserOptions` property like this:

```js
export default {
  // other rules...
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module',
    project: ['./tsconfig.json', './tsconfig.node.json'],
    tsconfigRootDir: __dirname,
  },
}
```

- Replace `plugin:@typescript-eslint/recommended` to `plugin:@typescript-eslint/recommended-type-checked` or `plugin:@typescript-eslint/strict-type-checked`
- Optionally add `plugin:@typescript-eslint/stylistic-type-checked`
- Install [eslint-plugin-react](https://github.com/jsx-eslint/eslint-plugin-react) and add `plugin:react/recommended` & `plugin:react/jsx-runtime` to the `extends` list
